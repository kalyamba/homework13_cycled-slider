document.addEventListener("DOMContentLoaded", function () {
  const images = document.querySelectorAll(".image-to-show");
  const totalImages = images.length;
  let currentIndex = 0;
  let intervalId;
  let countdown = 3;
  const timerElement = document.getElementById("timer");
  const stopButton = document.getElementById("stop-button");
  const resumeButton = document.getElementById("resume-button");
  let timerId;

  function showNextImage() {
    const currentImage = images[currentIndex];
    const nextIndex = (currentIndex + 1) % totalImages;
    const nextImage = images[nextIndex];

    fadeOut(currentImage);

    setTimeout(() => {
      currentImage.style.display = "none";
      fadeIn(nextImage);
    }, 500);

    currentIndex = nextIndex;
  }

  function startSlideshow() {
    clearInterval(intervalId);
    intervalId = setInterval(showNextImage, 3000);
  }

  startSlideshow();

  stopButton.addEventListener("click", () => {
    clearInterval(intervalId);
    clearInterval(timerId);
  });

  resumeButton.addEventListener("click", () => {
    startSlideshow();
    startTimer();
  });

  function fadeOut(element) {
    let opacity = 1;
    const timer = setInterval(function () {
      if (opacity <= 0.1) {
        clearInterval(timer);
        element.style.opacity = 0;
        element.style.display = "none";
      }
      element.style.opacity = opacity;
      opacity -= opacity * 0.1;
    }, 50);
  }

  function fadeIn(element) {
    let opacity = 0;
    element.style.display = "block";
    const timer = setInterval(function () {
      if (opacity >= 0.9) {
        clearInterval(timer);
        element.style.opacity = 1;
      }
      element.style.opacity = opacity;
      opacity += 0.1;
    }, 50);
  }

  function updateTimer() {
    timerElement.textContent = `Осталось: ${countdown.toFixed(1)} секунд`;
  }

  function startTimer() {
    updateTimer();

    timerId = setInterval(() => {
      countdown -= 0.1;

      if (countdown <= 0) {
        clearInterval(timerId);
        showNextImage();
        countdown = 3;
        startTimer();
      } else {
        updateTimer();
      }
    }, 100);
  }

  startTimer();
});
